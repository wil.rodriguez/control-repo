class profile::netdata {

  exec { 'download_kickstart':
    command  => 'curl -JLO https://my-netdata.io/kickstart-static64.sh && chmod +x /root/kickstart-static64.sh',
    cwd      => '/root',
    provider => shell,
    creates  => '/root/kickstart-static64.sh',
  }
  
  exec { 'install_netdata':
    command  => '/root/kickstart-static64.sh --accept',
    cwd      => '/root',
    provider => shell,
    creates  => '/opt/netdata',
  }
}