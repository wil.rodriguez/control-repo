class profile::base {
  $homedir = '/home/bob'
  service { 'httpd':
    ensure => stopped,
    before => Package['httpd'],
  }

  package { 'httpd':
    ensure => absent,
  }

  package { ['tree', 'bash-completion', 'vim-enhanced']:
    ensure => installed,
  }

  file { ["${homedir}/mydir", "${homedir}/mydir/things"]:
    ensure => directory,
  }

  group { 'bob':
    ensure => present,
  }

  user { 'bob':
    ensure     => present,
    home       => $homedir,
    managehome => true,
  }

  user { 'root':
    ensure             => 'present',
    comment            => 'Hi I\'m Groot',
    gid                => 0,
    home               => '/root',
    password           => '$6$vtI6Acjx$rvwYtIr5xPxyVJ0b9McRW7t5j8a2qK8KJeI8drelZ.U.iwA5YOutftY4GRA8Z1XHwL76SQoEWNB2/RfFjvmEx/',
    password_max_age   => -1,
    password_min_age   => 0,
    password_warn_days => 7,
    shell              => '/bin/bash',
    uid                => 0,
  }

}
